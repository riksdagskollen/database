create database ProjectFreedom

go

use ProjectFreedom

go --CREATE TABLES

CREATE TABLE dokument (
hangar_id int,
dok_id nvarchar(255),
rm nvarchar(255),
beteckning nvarchar(255),
doktyp nvarchar(255),
typ nvarchar(255),
subtyp nvarchar(255),
tempbeteckning nvarchar(255),
organ nvarchar(255),
mottagare nvarchar(255),
nummer int,
slutnummer int,
datum datetime,
systemdatum datetime,
publicerad datetime,
titel nvarchar(255),
subtitel nvarchar(255),
status nvarchar(255),
htmlformat nvarchar(255),
relaterat_id nvarchar(255),
source nvarchar(255),
sourceid nvarchar(255),
dokument_url_text nvarchar(255),
dokument_url_html nvarchar(255),
dokumentstatus_url_xml nvarchar(255),
utskottsforslag_url_xml nvarchar(255),
html ntext
);


CREATE TABLE dokutskottsforslag (
rm nvarchar(255),
bet nvarchar(255),
punkt int,
beteckning nvarchar(255),
rubrik nvarchar(255),
forslag nvarchar(255),
forslag_del2 nvarchar(255),
beslutstyp nvarchar(255),
beslut nvarchar(255),
motforslag_nummer int,
motforslag_partier nvarchar(255),
votering_id nvarchar(255),
votering_sammanfattning_html nvarchar(255),
votering_ledamot_url_xml nvarchar(255),
vinnare nvarchar(255)
);


CREATE TABLE dokmotforslag (
nummer int,
rubrik nvarchar(255),
forslag nvarchar(255),
partier nvarchar(255),
typ nvarchar(255),
utskottsforslag_punkt int,
id nvarchar(255)
);


CREATE TABLE dokaktivitet (
hangar_id int,
kod nvarchar(255),
namn nvarchar(255),
datum datetime,
status nvarchar(255),
ordning nvarchar(255),
process nvarchar(255)
);


CREATE TABLE dokintressent (
hangar_id int,
intressent_id nvarchar(255),
namn nvarchar(255),
partibet nvarchar(255),
ordning int,
roll nvarchar(255)
);


CREATE TABLE dokforslag (
hangar_id int,
nummer int,
beteckning nvarchar(255),
lydelse nvarchar(255),
lydelse2 nvarchar(255),
utskottet nvarchar(255),
kammaren nvarchar(255),
behandlas_i nvarchar(255),
kammarbeslutstyp nvarchar(255)
);

CREATE TABLE dokuppgift (
hangar_id int,
kod nvarchar(255),
namn nvarchar(255),
text ntext
);


CREATE TABLE dokbilaga (
hangar_id int,
dok_id nvarchar(255),
titel nvarchar(255),
subtitel nvarchar(255),
filnamn nvarchar(255),
filstorlek int,
filtyp nvarchar(255),
fil_url nvarchar(255)
);


CREATE TABLE dokreferens (
hangar_id int,
referenstyp nvarchar(255),
uppgift nvarchar(255),
ref_dok_id nvarchar(255),
ref_dok_typ nvarchar(255),
ref_dok_rm nvarchar(255),
ref_dok_bet nvarchar(255),
ref_dok_titel nvarchar(255),
ref_dok_subtitel nvarchar(255)
);


CREATE TABLE debatt (
hangar_id int,
video_id nvarchar(255),
video_url nvarchar(255),
tumnagel nvarchar(255),
anf_video_id nvarchar(255),
anf_hangar_id int,
anf_sekunder int,
anf_klockslag nvarchar(255),
datumtid datetime,
talare nvarchar(255),
anf_datum datetime,
anf_typ nvarchar(255),
anf_text nvarchar(255),
anf_beteckning nvarchar(255),
anf_nummer nvarchar(255),
intressent_id nvarchar(255),
parti nvarchar(255),
anf_rm nvarchar(255)
);


CREATE TABLE votering (
rm nvarchar(255),
beteckning nvarchar(255),
hangar_id int NOT NULL,
votering_id nvarchar(255) NOT NULL,
punkt int,
namn nvarchar(255),
intressent_id nvarchar(255) NOT NULL,
parti nvarchar(255),
valkrets nvarchar(255),
valkretsnummer int,
iort nvarchar(255),
rost nvarchar(255),
avser nvarchar(255),
votering nvarchar(255),
banknummer int,
fornamn nvarchar(255),
efternamn nvarchar(255),
kon nvarchar(255),
fodd int,
datum datetime
);


CREATE TABLE anforande (
pk int,
dok_id varchar(50),
dok_titel varchar(255),
dok_hangar_id int,
dok_rm varchar(20),
dok_nummer int,
dok_datum datetime,
avsnittsrubrik varchar(255),
kammaraktivitet varchar(250),
justerat char(1),
anf_id varchar(50),
anf_nummer int,
talare varchar(250),
rel_dok_id varchar(50),
parti varchar(50),
lydelse ntext,
intressent_id varchar(50),
intressent_hangar_id int,
replik char(1),
systemdatum datetime,
k�lla varchar(50),
anf_hangar_id int,
rel_dok_hangar_id int
);


CREATE TABLE person (
id int,
hangar_id int,
intressent_id varchar(20) NOT NULL,
kontrollsumma varchar(50),
f�dd_�r smallint,
f�dd datetime,
avliden datetime,
k�n varchar(6),
f�rnamn varchar(50),
efternamn nvarchar(50),
tilltalsnamn nvarchar(50),
sorteringsnamn varchar(80),
iort varchar(40),
parti varchar(40),
valkrets varchar(50),
b�nknummer int,
invalsordning int,
status varchar(100),
k�lla varchar(20),
k�lla_id varchar(40),
statsr�d varchar(50),
timestamp datetime,
personid int
);


CREATE TABLE personuppdrag (
id int,
organ_kod varchar(20),
hangar_id int,
intressent_id varchar(20),
ordningsnummer int,
roll_kod varchar(40),
status varchar(20),
typ varchar(20),
[from] datetime,
tom datetime,
k�lla varchar(30),
k�lla_id varchar(40),
uppgift varchar(500)
);


CREATE TABLE personuppgift (
id int,
hangar_id int,
intressent_id varchar(50),
uppgift_kod varchar(50),
uppgift ntext,
k�lla varchar(50),
k�lla_id varchar(50),
uppgift_typ varchar(50)
);


CREATE TABLE planering (
nyckel int,
id varchar(50),
rm varchar(12),
typ varchar(40),
dokserie_id char(2),
subtyp varchar(40),
bet varchar(40),
tempbet varchar(40),
intressent varchar(80),
nummer int,
slutnummer int,
datum datetime,
publicerad datetime,
status varchar(40),
titel nvarchar(300),
subtitel nvarchar(255),
html ntext,
toc ntext,
refcss nvarchar(66),
url nvarchar(100),
uppdaterad datetime,
storlek int,
source varchar(20),
wn_expires datetime,
wn_cachekey varchar(50),
wn_status varchar(20),
wn_checksum varchar(40),
wn_nid int,
wn_RawUrl varchar(255),
wn_SourceID varchar(80),
timestamp datetime,
rel_id varchar(50),
klockslag varchar(10),
grupp varchar(20),
format varchar(20),
intressent_id char(13),
mottagare_id char(13),
mottagare varchar(80),
hangar_id int,
plats varchar(150),
slutdatum datetime,
webbtvlive tinyint
);


CREATE TABLE organ (
id int,
kod varchar(50),
namn varchar(100),
typ varchar(50),
status varchar(12),
sortering int,
namn_en varchar(100),
dom�n varchar(50),
beskrivning varchar(1000)
);


CREATE TABLE roll (
pk int,
kod varchar(50),
namn varchar(100),
sort int
);


CREATE TABLE riksmote (
pk int,
riksmote varchar(20),
id varchar(3),
start datetime,
slut datetime,
mandatperiod varchar(20)
);

create table Ledamot(
	[intressent_id] varchar(20) NOT NULL
      ,[parti] nvarchar(2) NOT NULL
      ,[rm] nvarchar(20) NOT NULL
      ,[Ja] decimal(5, 2)
      ,[Nej] decimal(5, 2)
      ,[Avst�r] decimal(5, 2)
      ,[Fr�nvarande] decimal(5, 2)
	  ,PRIMARY KEY ([intressent_id], [rm], [parti])
)

create table LedamotProcent(
	   [intressent_id] varchar(20) NOT NULL
      ,[parti] nvarchar(2) NOT NULL
      ,[Fr�nvarande] decimal(5, 2)
	  ,[fornamn] varchar(50)
	  ,[efternamn] nvarchar(50)
	  ,PRIMARY KEY ([intressent_id], [parti])
)

create table Parti(
      [parti] varchar(2) NOT NULL
      ,[rm] varchar(20) NOT NULL
      ,[Ja] decimal(5, 2)
      ,[Nej] decimal(5, 2)
      ,[Avst�r] decimal(5, 2)
      ,[Fr�nvarande] decimal(5, 2)
	  ,PRIMARY KEY ([parti], [rm])
)

create table PartiProcent(
	  [Parti] varchar(2) NOT NULL
      ,[Riksdags�r] varchar(20) NOT NULL
      ,[Procent fr�nvaro] decimal(5, 2)
	  ,PRIMARY KEY ([parti], [Riksdags�r])
)

go --Updates

create procedure Ledamot_Update AS
Begin   
		truncate table Ledamot
		;with MyCTE (intressent_id, Riksdags�r, Ja, Nej, Avst�r, Fr�nvarande) as
		(
		select
			intressent_id,
			rm,
			count(*) as Ja,
			count(*) as Nej,
			count(*) as Avst�r,
			count(*) as Fr�nvarande

		from
			votering
		group by
			intressent_id,
			rm
	)
	insert into
		Ledamot
	select
		v.intressent_id,
		v.parti,
		v.rm,
		cast( count(case when rost = 'Ja' then 1 else null end)  * 100.0 / MyCTE.Ja as decimal(5,2)) as [Ja],
		cast( count(case when rost = 'Nej' then 1 else null end)  * 100.0 / MyCTE.Nej as decimal(5,2)) as [Nej],
		cast( count(case when rost = 'Avst�r' then 1 else null end)  * 100.0 / MyCTE.Avst�r as decimal(5,2)) as [Avst�r],
		cast( count(case when rost = 'Fr�nvarande' then 1 else null end)  * 100.0 / MyCTE.Fr�nvarande as decimal(5,2)) as [Fr�nvarande]
	from
		votering as v
	inner join
		MyCTE on
		MyCTE.intressent_id = v.intressent_id and
		MyCTE.Riksdags�r = v.rm
	group by
		v.intressent_id,
		v.parti,
		v.rm,
		MyCTE.Ja,
		MyCTE.Nej,
		MyCTE.Avst�r,
		MyCTE.Fr�nvarande
End


go

create procedure LedamotProcent_Update AS
Begin   
		truncate table LedamotProcent
		;with MyCTE (intressent_id, Fr�nvarande) as
		(
		select
			intressent_id,
			count(*) as Fr�nvarande
		from
			votering
		group by
			intressent_id
	)
	insert into
		LedamotProcent (intressent_id, parti, Fr�nvarande)
	select
		v.intressent_id,
		v.parti,
		cast( count(case when rost = 'Fr�nvarande' then 1 else null end)  * 100.0 / MyCTE.Fr�nvarande as decimal(5,2)) as [Fr�nvarande]
	from
		votering as v
	inner join
		MyCTE on
		MyCTE.intressent_id = v.intressent_id
	group by
		v.intressent_id,
		v.parti,
		MyCTE.Fr�nvarande
	update
		LedamotProcent
	SET 
		fornamn = p.fornamn,
		efternamn = p.efternamn
	from votering as p
	where 
	LedamotProcent.intressent_id = p.intressent_id
End


go

create procedure Parti_Update AS   
Begin
		truncate table Parti
		;WITH MyCTE (Parti, Riksdags�r, Ja, Nej, Avst�r, Fr�nvarande) AS 
			(
		SELECT
			parti,
			rm,
			count(*) as Ja,
			count(*) as Nej,
			count(*) as Avst�r,
			count(*) as Fr�nvarande
		FROM
			dbo.votering
		GROUP BY
			parti,
			rm
	)
	insert into
		Parti
	SELECT
		v.parti,
		v.rm,
		cast( count(case when rost = 'Ja' then 1 else null end)  * 100.0 / MyCTE.Ja as decimal(5,2)) as [Ja],
		cast( count(case when rost = 'Nej' then 1 else null end)  * 100.0 / MyCTE.Nej as decimal(5,2)) as [Nej],
		cast( count(case when rost = 'Avst�r' then 1 else null end)  * 100.0 / MyCTE.Avst�r as decimal(5,2)) as [Avst�r],
		cast( count(case when rost = 'Fr�nvarande' then 1 else null end)  * 100.0 / MyCTE.Fr�nvarande as decimal(5,2)) as [Fr�nvarande]
	FROM
		dbo.votering AS v
	INNER JOIN
		MyCTE ON 
		MyCTE.Parti = v.parti and
		MyCTE.Riksdags�r = v.rm

	GROUP BY
		v.parti,
		v.rm,
		MyCTE.Ja,
		MyCTE.Nej,
		MyCTE.Avst�r,
		MyCTE.Fr�nvarande
End


go

create procedure PartiProcent_Update AS   
Begin

	truncate table Partiprocent

;WITH [All] ([Parti], [Riksdags�r], [Total]) AS 
	(
		SELECT
			parti,
			rm,
			COUNT(*) AS Expr1
		FROM
			dbo.votering
		GROUP BY
			parti,
			rm
	), Absent([Parti], [Riksdags�r], [Fr�nvarande]) AS
	(
		SELECT
			parti,
			rm,
			COUNT(*) AS Expr1
		FROM
			dbo.votering
		WHERE
			(rost = 'Fr�nvarande')
		GROUP BY
			parti,
			rm
	)
	insert into
		Partiprocent
	SELECT
		upper(v.parti) AS Parti,
		v.rm as [Riksdags�r],
		cast([Absent].[Fr�nvarande] * 1.0 / [All].[Total] * 100.0 as Decimal(5,2)) as [Procent fr�nvaro]
	FROM
		dbo.votering AS v
	INNER JOIN
		[All] ON 
		[All].[Parti] = v.parti and
		[All].Riksdags�r = v.rm
	INNER JOIN
		[Absent] ON
		[Absent].[Parti] = v.parti and
		[Absent].Riksdags�r = v.rm
	GROUP BY
		v.parti, [All].[Total],
		[Absent].[Fr�nvarande],
		v.rm,
		[Absent].[Fr�nvarande] / [All].[Total]
End